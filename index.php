<?php

include "./models/instagram_api.php";

use Accie\Api\InstagramApi;

$instagram_api = new InstagramApi();
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">

    <title>Instagram Dashboard</title>
</head>

<body>

<header>
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1>Instagram Dashboard</h1>
            <p class="lead">Shows your 12 recent pictures</p>
        </div>
    </div>
</header>
<main class="container">
    <div class="search-container">
        <form method="post" action="index.php" autocomplete="off">
            <div class="row">
                <div class="col-12 col-md-6 offset-md-3">
                    <div class="form-group">
                        <small for="usernameInput">
                            <?php if (empty($_POST["usernameInput"])) {
                                print_r($instagram_api->give_error());
                            } ?>
                        </small>
                        <input type="text" class="form-control usernameInput" name="usernameInput"
                               placeholder="username"/>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="form-control btn-dark" value="Search"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="img-container">
        <div class="row">
            <?php
            if (!empty($_POST["usernameInput"])) {
                $instagram_api->setUsername($_POST["usernameInput"]);
            }
            ?>
        </div>
    </div>
</main>
<footer class="container-fluid">
    <div class="row">

    </div>
</footer>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script src="js/script.js"></script>
</body>

</html>