$(function () {
    const img = $(".img-container img");

    /**
     * De effecten voor het hoveren over de content
     */
    img.each(function (e) {
        $(this).on('mouseenter', function () {
            $(this).addClass('animation');
        });
        $(this).on('mouseleave', function () {
            $(this).removeClass('animation');
        });
    });

});