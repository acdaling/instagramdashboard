<?php
/**
 * Deze class scrapt de instagram pagina van de opgegeven gebruikersnaam en die data wordt uit een
 * tekstbestand opgehaald.
 *
 * Created by Acdaling Edusei.
 */

namespace Accie\Api;

class InstagramApi
{
    private $url = "https://www.instagram.com/";
    private $username;

    public function setUsername($username)
    {
        $this->username = $username;
        $this->handelContent();
    }

    /**
     * @param bool $refetchData Het opnieuw voeren van een url request om zo de data weer
     *  op te halen, wanneer false gebruikt het de data.txt
     */
    private function handelContent($refetchData = true)
    {
        $dataFromUser = $this->url . $this->username;
        /** slaat de opgehaalde data op in een tekstbestand */
        if (!file_exists("data.txt") || $refetchData) {
            $userData = file_get_contents($dataFromUser);
            file_put_contents("data.txt", $userData);
        }

        /** haalt de data uit de tekstbestand op en slaat het op in een variabel*/
        $getStoredData = file_get_contents("data.txt");

        /**
         * preg_match, zoekt naar een stuk code die gelijk is aan de pattern
         * Er is gebruikt gemaakt van '#' inplaats van '/', omdat er in de pattern ook
         * van zulke tekens in zitten
         *
         * Als er een match is dan zet die de gevonden data om in een array
         */
        if (preg_match('/window._sharedData = (.*?)script>/s', $getStoredData, $matches)) {
            if (count($matches) > 0) {
                $match = substr($matches[1], 0, -3);
                $decode = json_decode($match, false);

                /**
                 * Allerlei gegevens over de gebruiker die gebruikt kunnen worden
                 */
                $userProfilePage = $decode->entry_data->ProfilePage[0]->graphql->user;
                $userProfilePicture = $userProfilePage->profile_pic_url;
                $userBio = $userProfilePage->biography;
                $userFullName = $userProfilePage->full_name;
                $userWebsite = $userProfilePage->external_url;//de website in de bio
                $userFollowedByCount = $userProfilePage->edge_followed_by;//aantal mensen dat mij volgen
                $userFollowsCount = $userProfilePage->edge_follow;//aantal mensen dat gevolgd wordt
                $userPostsTimeLine = $userProfilePage->edge_owner_to_timeline_media;
                $userPostCount = $userPostsTimeLine->count; //het aantal foto's/video's dat geplaatst is


                /**
                 * De berichten kunnen displayen doormiddel van een loop
                 * en de index van de bericht
                 */
                for ($intIndex = 0; $intIndex < count($userPostsTimeLine->edges); $intIndex++) {
                    /** @var $shortner maakt een kortere pad voor het variabel die de gegevens van de media bevat */
                    $shortner = $userPostsTimeLine->edges[$intIndex]->node;
                    /** @var  $userPostImgSource de source van de afbeelding van de userprofiel */
                    $userPostImgSource = $userPostsTimeLine->edges[$intIndex]->node->thumbnail_src;
                    /** @var  $userCommentsCount */
                    $userCommentsCount = $shortner->edge_media_to_comment->count;
                    $userPostDate = $shortner->taken_at_timestamp;

                    /**
                     * In deze versie van de applicatie is het niet mogelijk om video's te displayen, daar moeten nog
                     * een paar aanpassingen voor gedaan worden.
                     */
                    if ($shortner->__typename != "GraphVideo") {
                        echo "<div class='col-12 col-sm-6 col-md-4'>";
                        echo '<a href="#" class="img-container"><img class="img-fluid" src="' . $userPostImgSource . '" /></a>';

//                            if (!empty($userPostBio) > 0) {
//                                echo "<time class='user_post_date' datetime='.$userPostDate.'>" . date("Y-m-d", $userPostDate) . "</time>";
//                            }
                        echo "</div>";
                    }
                }
            }
        }
    }


    public function give_error()
    {
        return (
           "Put in a username"
        );
    }

    public function username_controll()
    {
        return (
            "Oeps, this user doesn't exist or the account is on private"
        );
    }
}